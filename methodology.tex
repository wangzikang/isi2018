\section{Methodology}

\input{figOverall}

The whole architecture of our proposed approach is shown in Fig \ref{figOverall}.
In the first phase, for each triple $(h, r, t)$, 
multiple reasoning paths can be found between head entity $h$ and 
tail entity $t$ through random walking.
These paths usually include more than one relation or entity, 
so are referred to as ``multi-hop'' paths. 
In general, every path is semantically related to the target relation
``$r$'' in a different level, while complementary to each other as well. 
To bring more flexibility and diversity to the path finding process,
we also take the inverse $r^{-1}$, from tail entity $t$ to head entity $h$, 
of relation $r$ into consideration. Thus, for every triple $(h,r,t)$, 
we add a triple $(t,r^{-1},h)$ into the knowledge graph, 
which allows each entity can be visited several times if required.

For a triple $(h, r, t)$, suppose we found $n$ paths, 
each consists of several entities and relations, 
denote the $i$-th path as $p_i$, 
$p_i$ can be represented as a list of ordered relations and entities:
\begin{align}
    p_i = h, r_1, e_1, r_2, e_2, \dots , r_m, t .
\end{align}
Where path $p_i$ starts from the head $h$, 
passes through entity $e_1, e_2, \dots$ via relations $r_1, r_2, \dots, r_m$ 
respectively, finally, arrives at the tail $t$.
The set of all found paths for this triple is denoted as 
$S={p_1, p_2, \dots, p_n}$.

For a query, say $q$, we also represent it by entities and relations, 
as shown in Fig \ref{figOverall}.
For fact prediction task, the query can be denoted in form $q_i=(h, r, t)$,
which is a path with two entities and a single relation
(The output for fact prediction task will be $1/0$, 
which indicates the triple is true or false respectively). 
For other tasks, the resulting forms of queries can still be 
denoted as paths consisting of entities and relations, 
but may be different from each other.
This structure not only provides a unified architecture to solve different tasks 
which increases flexibility of the model, but also offers the opportunity
to guide the learning process by the query to achieve better results.

To better represent the semantic knowledge of different paths, 
we initialize the entity and relation representations with pretrained embeddings. 
Pretraining usually uses fundamental representation learning methods, 
such as TransE\cite{transe}, TransH\cite{transh}, etc.
Next, we compute the attention for each path based on embeddings.
Then we combine these paths based on their attentions to get an
aggregated embedding through average or summation of the paths' embeddings.
The final answer to a specific task can be obtained based on this
aggregated embedding and the query's embedding.

Let $A$ denote the pretrained embedding matrix, 
then the representation for path $p_i$ can be computed as 
\begin{align}
    r_i = \sum_{j}Ap_{ij},
\end{align}
where $j$ runs through all entities and relations included in path $p_i$.
To encode the order information into representations, 
we add position encoding to the representation $r_i$:
 \begin{align}
     i = \sum_{j} l_j \cdot Ap_{ij},
\end{align}
where $l_j$ is a vector\cite{n2nmem} with the following definition
\begin{align}
    l_{jk} = (1-j/J) - (k/d)(1-2j/J), ~~ k=1, 2, \cdots, J
\end{align}
where $J$ is the total number of entities and relations in the path.
The query will be represented in the same way.

As different paths carry different knowledge, 
each related to the query in a different level.
Thus, it is nature to give each path a different attention while combing them.
For query $q$, attention $a_i$ for path $p_i$ can be calculated as
\begin{align} 
    a_i = \text{softmax}(q^{T}r_i).
\end{align}
Combining different paths according to their attentions,
then we can get the aggregated representation $p$ as:
\begin{align} 
    p = \sum_{i}a_{i}p_{i}.
\end{align}

After getting the aggregated representation of different paths,
we can construct the final prediction with respect to the query $q$ by
\begin{align} 
    o = \text{softmax}(W(p+q)),
\end{align}
where $W$ is the weight matrix,
output $o$ is the final prediction respecting to query $q$.
Besides, the similarity score $s$ of the aggregated path and target relation
$r$ is
\begin{align}
    s = \text{normalize}(W(p+q)),
\end{align}
again $W$ is the weight matrix.

For each query, the loss is defined as the crossing entropy between 
the ground truth and our model's prediction,
we then minimize the loss function with stochastic gradient descent (SGD) algorithm.
